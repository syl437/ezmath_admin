import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    public folderName:string = 'users_debt';
    public addButton:string = 'הוסף משתמש'
    deleteModal: any;
    detailsModal: any;
    selectedItem: any;
    companyToDelete: any;
    SubCatId:any;
    UserType :any = ["תלמיד","מורה",'מנהל'];
    pushModal: any;
    companyToPush: any;
    pushText: string = '';
    sent: boolean = false;
    inProcess: boolean = false;


    constructor(public MainService: MainService, settings: SettingsService , private modalService: NgbModal , private route: ActivatedRoute, public router:Router) {
        this.route.params.subscribe(params => {
            this.SubCatId = params['id'];
            if(!this.SubCatId)
                this.SubCatId = "-1";
            console.log("11 : " , this.SubCatId)
            this.host = settings.host;
            this.avatar = settings.avatar;
            this.getItems();
        });
    }

    ngOnInit() {
    }

    getItems()
    {
        this.MainService.GetItems('getUsersDebt', this.SubCatId ).then((data: any) => {
            console.log("getUsersDebt : ", data)
            this.ItemsArray = data;
            this.ItemsArray1 = data;
        })
    }

    DeleteItem() {
        this.MainService.DeleteItem('deleteUserDebt', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
            this.getItems();
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.student_name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

    openDetailsModal(content, item){
        console.log("DM : " , content , item)
        this.detailsModal = this.modalService.open(content);
        this.selectedItem = item;
    }

    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    openPushModal (content, company) {
        this.pushModal = this.modalService.open(content);
        this.companyToPush = company;
    }

    closePushModal () {
        this.pushModal.close();
        this.sent = false;
    }

    sendPush() {

        this.inProcess = true;
        this.sent = true;

        this.MainService.SendPush('WebSendUserPush', this.companyToPush.id,this.pushText).then((data: any) => {
            this.pushText = '';
            this.sent = true;
            this.inProcess = false;
            this.closePushModal();
        })
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }

    openTicketsPage(user_id,has_group) {
        if (has_group == 0) {
            alert ("יש תחילה לשייך משתמש זה לקבוצת כרטיסיות")
        }
        else {
            this.router.navigate(['/','user_tickets','index'], { queryParams: { user_id: user_id } });
        }
    }
}
