import {Component, OnInit, ViewChild} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    ProductId;
    subCat;
    host: string = '';
    // settings = '';
    // avatar = '';
    // public folderName:string = 'products';
    // public addButton:string = 'הוסף קטגורייה'
    // deleteModal: any;
    // detailsModal: any;
    // selectedItem: any;
    // companyToDelete: any;
    // SubCatId:any;
    // Category:any;
    @ViewChild("fileInput") fileInput;

    constructor(public MainService: MainService,public service: MainService, settings: SettingsService , private modalService: NgbModal , private route: ActivatedRoute) {
        this.route.params.subscribe(params => {
            this.ProductId = params['id'];
            if(!this.ProductId)
                this.ProductId = "-1";

            this.subCat = params['subCat'];
            console.log("11 : " , this.ProductId)
            this.MainService.GetItems('getImages', this.ProductId ).then((data: any) => {
                console.log("GetCategories12 : ", data)
                    this.ItemsArray = data;
                    this.ItemsArray1 = data;
                    this.host = settings.host;
            })
        });
    }

    ngOnInit() {
    }

    onChange(event) {
        console.log(event)
        var files = event.target.files[0]; //event.srcElement.files;

        // let reader = new FileReader();
        // reader.onload = (e: any) => {
        //     this.changeImage = e.target.result;
        // }
        //
        // reader.readAsDataURL(event.target.files[0]);

        let fi = this.fileInput.nativeElement;
        let fileToUpload;
        if (fi.files && fi.files[0]) {fileToUpload = fi.files[0]; console.log("fff : ",fileToUpload);}

        //this.Item.change = this.Change;
        this.service.AddItem('AddImage',this.ProductId, fileToUpload).then((data: any) => {
            console.log("GetCategories11 : ", data)
            this.ItemsArray = data;
        });
    }
    

    deleteItem(i)
    {
        console.log("I : " , i ,  this.ProductId)
        this.service.DeleteItem('DeleteImage',i , this.ProductId).then((data: any) => {
            console.log("GetCategories11 : ", data)
            this.ItemsArray = data;
        });
    }

    // DeleteItem() {
    //     this.MainService.DeleteItem('DeleteProduct', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
    //         this.ItemsArray = data , console.log("Del 2 : ", data);
    //     })
    // }
    //
    // updateFilter(event) {
    //     const val = event.target.value;
    //     // filter our data
    //     const temp = this.ItemsArray1.filter(function (d) {
    //         return d.title.toLowerCase().indexOf(val) !== -1 || !val;
    //     });
    //     // update the rows
    //     this.ItemsArray = temp;
    // }
    //
    // openDetailsModal(content, item){
    //     console.log("DM : " , content , item)
    //     this.detailsModal = this.modalService.open(content);
    //     this.selectedItem = item;
    // }
    //
    // openDeleteModal(content,index)
    // {
    //     this.deleteModal = this.modalService.open(content);
    //     this.companyToDelete = index;
    // }
    //
    // async deleteCompany()
    // {
    //     this.deleteModal.close();
    //     this.DeleteItem();
    //     console.log("Company To Delete : " , this.companyToDelete)
    // }

}
