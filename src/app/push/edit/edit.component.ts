import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder,FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {Ng2UploaderModule} from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";



@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent implements OnInit {
    registerForm: FormGroup;
    public Id;
    public navigateTo: string = '/users/index';
    public imageSrc: string = '';
    public Items: any[] = [];
    public changeImage;
    public rowsNames: any[] = ['שם התלמיד','שם משתמש','טלפון','אימייל','עיר','שם בית ספר','שם הורה','טלפון הורה'];
    public rows: any[] = ['student_name','username','phone','email','city','scool_name','parent_name','parent_phone'];
    public Item;
    // public Item = {
    //     CategoryName: "Clothing",
    //     FullPath:"http://www.tapper.co.il/dagim/laravel/storage/app/public/uploads/112417-201124_2.jpg",
    //     SubCategoryName:"mens",
    //     category_id: "5",
    //     date: "2017-11-24 20:16:24",
    //     description: "Styles available in our Big & Tall range can be identified by this symbol. Designed to fit sizes up to XXXXL, 46” waist and 38” leg plus shoes available in sizes up to 13. Extra length is added in the sleeve, body and leg of garments ensuring you never miss out on the latest trends.",
    //     description_english: "Styles available in our Big & Tall range can be identified by this symbol. Designed to fit sizes up to XXXXL, 46” waist and 38” leg plus shoes available in sizes up to 13. Extra length is added in the sleeve, body and leg of garments ensuring you never miss out on the latest trends.",
    //     details_image: "uploads/112417-201124_2.jpg",
    //     high_price: "15",
    //     id: 22,
    //     image: "uploads/112417-201124_2.jpg",
    //     low_price: "15",
    //     sub_category_id: "7",
    //     title: "Grey Long Sleeve Fine Stripe Grandad",
    //     title_english: "Grey Long Slee",
    //     change : ""};

    public host;
    public Image;
    public SubCategories;
    public isReady;
    public Change: boolean = false;
    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute, private http: Http, public service: MainService, public router: Router, public settings: SettingsService) {

        this.route.params.subscribe(params => {
            this.Id = params['id'];
            this.Item = this.service.Items[this.Id];
            this.host = settings.host;
            //this.Item.Change  = false;
            //this.isReady = true;
            console.log("Product", this.Item)
            console.log("Item : " , this.Item)
        });
    }

    onSubmit(form: NgForm) {


        let fi = this.fileInput.nativeElement;
        let fileToUpload;
        if (fi.files && fi.files[0]) {fileToUpload = fi.files[0]; console.log("fff : ",fileToUpload);}
        this.Item.Change = this.Change;

        console.log("EditUser",form.value);

        this.service.EditItem('EditUser',form.value,fileToUpload).then((data: any) => {
            console.log("AddUser : " , data);
            this.router.navigate([this.navigateTo]);
        });

    }

    ngOnInit() {

        this.registerForm = new FormGroup({
            'id':new FormControl(this.Item.id),
            'Change':new FormControl(this.Item.Change),
            'username':new FormControl(this.Item.username,Validators.required),
            //'password':new FormControl(null,Validators.required),
            'student_name':new FormControl(this.Item.student_name,Validators.required),
            'phone':new FormControl(this.Item.phone,[Validators.required, Validators.minLength(9),Validators.pattern('^[0-9]+$')]),
            'email':new FormControl(this.Item.email,[Validators.required, Validators.email]),
            'city':new FormControl(this.Item.city,Validators.required),
            'schoolgrade':new FormControl(this.Item.schoolgrade,[Validators.required, Validators.minLength(1)]),
            'teachinglevel':new FormControl(this.Item.teachinglevel,[Validators.required, Validators.minLength(1)]),
            'scool_name':new FormControl(this.Item.scool_name,Validators.required),
            'parent_name':new FormControl(this.Item.parent_name,Validators.required),
            'parent_phone':new FormControl(this.Item.parent_phone,[Validators.required, Validators.minLength(9),Validators.pattern('^[0-9]+$')]),
        })

    }


    onChange(event) {

        this.Change = true;
        var files = event.srcElement.files;
        this.changeImage = event.target.files[0];

        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.changeImage = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
}
