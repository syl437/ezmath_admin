
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {SettingsService} from "../../settings/settings.service";



//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
@Injectable()

export class MainService
{
    public ServerUrl = "";
    public headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    public options = new RequestOptions({headers:this.headers});

    public Items:any[]=[];
    public Branches:any[]=[];
    public Professions:any[]=[];

    constructor(private http:Http ,  settings:SettingsService)
    {
        this.ServerUrl = settings.ServerUrl;
    };

    GetItems(url:string ,Id,date)
    {
        let body = new FormData();
        body.append('id', Id);
        body.append("date", JSON.stringify(date));
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Items = data }).toPromise();
    }

    GetBranches(url:string)
    {
        let body = new FormData();
        //body.append('id', Id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Branches = data }).toPromise();
    }

    GetProfessions(url:string)
    {
        let body = new FormData();
        //body.append('id', Id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Professions = data }).toPromise();
    }


    AddItem(url,Item,File)
    {
        this.Items = Item;
        let body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        return this.http.post (this.ServerUrl + '' + url, body).map(res => res).do((data)=>{}).toPromise();
    }

    EditItem(url,Item,File)
    {
        console.log("IT : " , File , Item)
        this.Items = Item;
        let body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        //let body = 'category=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
    }

    DeleteItem(url,Id)
    {
        let body = 'id=' + Id;
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(res => res.json()).do((data)=>{}).toPromise();
    }
};


