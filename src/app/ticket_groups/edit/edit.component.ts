import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder,FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {Ng2UploaderModule} from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";



@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent implements OnInit {
    registerForm: FormGroup;
    public Id;
    public navigateTo: string = '/ticket_groups/index';
    public imageSrc: string = '';
    public Items: any[] = [];
    public changeImage;
    //public rowsNames: any[] = ['שם התלמיד','שם משתמש','סיסמה','טלפון','אימייל','עיר','שם בית ספר','שם הורה','טלפון הורה'];
    //public rows: any[] = ['student_name','username','password','phone','email','city','scool_name','parent_name','parent_phone'];
    public Item;


    public host;
    public Image;
    public SubCategories;
    public isReady;
    public rowIndex:any = '';
    public Change: boolean = false;
    public fields:any = {
        "id" : "",
        "title" : "",
        "selected_students" : []
    }
    public StudentsArray:any = [];
    errors: Array<any> = [];
    dropdownSettings = {
        singleSelection: false,
        text:"משתמשים/תלמידים",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        classes:"custom-select-item",
        enableSearchFilter: true,
        //limitSelection : 4,
    };

    @ViewChild("fileInput") fileInput;


    constructor(private route: ActivatedRoute, private http: Http, public service: MainService, public router: Router, public settings: SettingsService) {

        this.route.params.subscribe(params => {
            this.Id = params['id'];

            for (let i = 0; i < this.service.Items.length; i++) {
                if (this.service.Items[i].id == this.Id) {
                    this.rowIndex = i;
                }
            }


            this.Item = this.service.Items[this.rowIndex];
            this.host = settings.host;
            //this.Item.Change  = false;
            //this.isReady = true;
            console.log("Product", this.Item)
            console.log("Item : " , this.Item)
        });
    }

    onSubmit(form: NgForm) {


        //let fi = this.fileInput.nativeElement;
        let fileToUpload;
        //if (fi.files && fi.files[0]) {fileToUpload = fi.files[0]; console.log("fff : ",fileToUpload);}
        //this.Item.Change = this.Change;

        //console.log("EditUser",form.value);

        this.service.AddItem('webEditTicketGroup',this.fields,fileToUpload).then((data: any) => {
            console.log("webEditTicketGroup : " , data);
            this.router.navigate([this.navigateTo]);
        });

    }

    getStudents(id) {
        this.service.GetItems('webGetStudentsForGroup',id).then((data: any) => {
            console.log("webGetStudentsForGroup : " , data);
            this.StudentsArray = data;
        });
    }

    getExistingStudentGroup(id) {
        this.service.GetItems('getExistingStudentGroup',id).then((data: any) => {
            console.log("getExistingStudentGroup : " , data);
            this.fields.selected_students = data;
        });
    }

    ngOnInit() {
        this.fields.id = this.Item.id;
        this.fields.title = this.Item.title;
        this.getStudents(this.Item.id);
        this.getExistingStudentGroup(this.Item.id);
    }


    onChange(event) {

        this.Change = true;
        var files = event.srcElement.files;
        this.changeImage = event.target.files[0];

        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.changeImage = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
}
