import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    public folderName:string = 'tickets';
    public addButton:string = ''
    deleteModal: any;
    detailsModal: any;
    selectedItem: any;
    companyToDelete: any;
    UserId:any;

    constructor(public MainService: MainService, settings: SettingsService , private modalService: NgbModal , private route: ActivatedRoute, public router:Router) {
        this.route.queryParams.subscribe(params => {
            this.UserId = params['user_id'];
            if (!this.UserId)
                this.UserId = "-1";
            console.log("11 : " , this.UserId)
            this.host = settings.host;
            this.avatar = settings.avatar;
            this.getItems();
        });
    }

    ngOnInit() {
    }

    getItems()
    {
        this.MainService.GetItems('WebgetUserTickets', this.UserId ).then((data: any) => {
            console.log("WebgetUserTickets : ", data)
            this.ItemsArray = data;
            this.ItemsArray1 = data;
        })
    }

    DeleteItem() {
        this.MainService.DeleteItem('webDeleteUserTicket', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
            this.getItems();
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.user_data.student_name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

    openDetailsModal(content, item){
        console.log("DM : " , content , item)
        this.detailsModal = this.modalService.open(content);
        this.selectedItem = item;
    }

    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }


    addTicketPage() {
        //this.router.navigate(['/','user_tickets','add'], { queryParams: { user_id: this.UserId } });
    }

}
