import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    public folderName:string = 'classes2';
    public addButton:string = 'הוסף שיעור'
    deleteModal: any;
    detailsModal: any;
    selectedItem: any;
    companyToDelete: any;
    SubCatId:any;
    now = new Date();
    searchDate:any;

    constructor(public MainService: MainService, settings: SettingsService , private modalService: NgbModal , private route: ActivatedRoute) {
        this.route.params.subscribe(params => {
            this.SubCatId = params['id'];
            if(!this.SubCatId)
                this.SubCatId = "-1";
            console.log("11 : " , this.SubCatId)
            this.host = settings.host;
            this.avatar = settings.avatar;
        });
    }

    ngOnInit() {
        this.searchDate = {year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate()};
        this.getItems(this.searchDate);
    }

    onDateChange(evt) {
        this.getItems(this.searchDate);
    }

    getItems(date)
    {
        this.MainService.GetItems('WebGetUserClasses', this.SubCatId ,date).then((data: any) => {
            console.log("WebGetUserClasses : ", data)
            this.ItemsArray = data;
            this.ItemsArray1 = data;
        })
    }

    DeleteItem() {
        this.MainService.DeleteItem('WebDeleteUserClass', this.ItemsArray[this.companyToDelete].itemId).then((data: any) => {
            this.getItems(this.searchDate);
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.fullname.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

    openDetailsModal(content, item){
        console.log("DM : " , content , item)
        this.detailsModal = this.modalService.open(content);
        this.selectedItem = item;
    }

    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }

}
