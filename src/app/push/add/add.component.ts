import {Component, OnInit, ElementRef, ChangeDetectionStrategy , ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormControl , FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";

import { FileUploader, FileUploaderOptions } from 'ng2-file-upload/ng2-file-upload';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {

    registerForm: FormGroup;
    public navigateTo:string = '/users/index';
    public paramsSub;
    public id: number;
    public SubCategories;
    public isReady;
    public imageSrc: string = '';
    public folderName:string = 'users';
    public rowsNames: any[] = ['שם התלמיד','שם משתמש','סיסמה','טלפון','אימייל','עיר','שם בית ספר','שם הורה','טלפון הורה'];
    public rows: any[] = ['student_name','username','password','phone','email','city','scool_name','parent_name','parent_phone'];
    public sub;



    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router) {
        console.log("Row : " , this.rows)
        this.route.params.subscribe(params => {
            this.sub = params['sub'];
        });
    }

    onSubmit(form:NgForm)
    {
        console.log(form.value);
        let fi = this.fileInput.nativeElement;
        let fileToUpload;
        if (fi.files && fi.files[0]) {fileToUpload = fi.files[0];}

        if(this.sub != -1)
        form.value.sub_category_id = this.sub;

        console.log(form.value)
        this.service.AddItem('AddUser',form.value,fileToUpload).then((data: any) => {
            console.log("AddUser : " , data);
            this.router.navigate([this.navigateTo]);
        });
    }
    
    ngOnInit() {
        this.paramsSub = this.route.params.subscribe(params => this.id = params['id']);

        this.registerForm = new FormGroup({
            'username':new FormControl(null,Validators.required),
            'password':new FormControl(null,Validators.required),
            'student_name':new FormControl(null,Validators.required),
            'phone':new FormControl(null,[Validators.required, Validators.minLength(9),Validators.pattern('^[0-9]+$')]),
            'email':new FormControl(null,[Validators.required, Validators.email]),
            'city':new FormControl(null,Validators.required),
            'schoolgrade':new FormControl(null,[Validators.required, Validators.minLength(1)]),
            'teachinglevel':new FormControl(null,[Validators.required, Validators.minLength(1)]),
            'scool_name':new FormControl(null,Validators.required),
            'parent_name':new FormControl(null,Validators.required),
            'parent_phone':new FormControl(null,[Validators.required, Validators.minLength(9),Validators.pattern('^[0-9]+$')]),
            'image':new FormControl(null),
        })
    }
    
    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }
}
