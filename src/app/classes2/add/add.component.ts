import {Component, OnInit, ElementRef, ChangeDetectionStrategy , ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormControl , FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";

import { FileUploader, FileUploaderOptions } from 'ng2-file-upload/ng2-file-upload';
import {NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {

    registerForm: FormGroup;
    public navigateTo:string = '/classes2/index';
    public paramsSub;
    public id: number;
    public SubCategories;
    public isReady;
    public imageSrc: string = '';
    public folderName:string = 'classes2';
    //public rowsNames: any[] = ['כותרת','כמות'];
    //public rows: any[] = ['title','quan'];
    now = new Date();
    time: NgbTimeStruct = {hour: 13, minute: 30, second: 30};


    public fields:any = {
        "title" : "",
        "max_students" : "1",
        "class_date" : "",
        "branchselect" : "",
        "professionselect" : "",
        "teacher_id" : "0",
        "class_time" : this.time,
        "class_total_hours" : "1"
    }



    public sub;
    public BranchesItems: any[];
    public ProfessionsItems: any[];
    public teachersArray: any = [];
    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router) {
        //console.log("Row : " , this.rows)
        this.route.params.subscribe(params => {
            this.sub = params['sub'];
        });
    }

    onSubmit(form:NgForm)
    {
        console.log(form.value);
        //let fi = this.fileInput.nativeElement;
        let fileToUpload;
        /*
        if (fi.files && fi.files[0]) {fileToUpload = fi.files[0];}

        if(this.sub != -1)
        form.value.sub_category_id = this.sub;
        */
        console.log(form.value)
        this.service.AddItem('AddClass',this.fields,fileToUpload).then((data: any) => {
            console.log("AddClass : " , data);
            this.router.navigate([this.navigateTo]);
        });
    }

    async getBranches()
    {
        await this.service.GetBranches('GetBranches').then((data: any) => {
            console.log("GetBranches : " , data);
            this.BranchesItems = data;
        });
    }

    async GetProfessions()
    {
        await this.service.GetProfessions('GetProfessions').then((data: any) => {
            console.log("GetProfessions : " , data);
            this.ProfessionsItems = data;
        });
    }

    async GetTeachers()
    {
        await this.service.GetTeachers('webGetTeachers').then((data: any) => {
            console.log("webGetTeachers : " , data);
            this.teachersArray = data;
        });
    }

    ngOnInit() {
        this.paramsSub = this.route.params.subscribe(params => this.id = params['id']);
        this.fields.class_date = {year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate()};
        this.getBranches();
        this.GetProfessions();
        this.GetTeachers();
    }

    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }
}
