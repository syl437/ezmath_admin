import {NgModule, Pipe} from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DragulaModule } from 'ng2-dragula/ng2-dragula';


import { MaindRoutes } from './Main.routing';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';



import {MainService} from "./MainService.service";
import {HttpModule} from "@angular/http";
import {LineaComponent} from "../icons/linea/linea.component";
import {SliComponent} from "../icons/sli/sli.component";
import { IndexComponent } from './index/index.component';
import {ButtonIconsComponent} from "../components/button-icons/button-icons.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FileUploadModule} from "ng2-file-upload";
import {FormsModule, NgControl, ReactiveFormsModule} from "@angular/forms";
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';




@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(MaindRoutes),
        NgxDatatableModule,
        HttpModule,
        NgbModule,
        FileUploadModule,
        FormsModule,
        ReactiveFormsModule,
        AngularMultiSelectModule
    ],
    declarations: [
        IndexComponent,
    ],
    providers: [MainService]
})


export class MainModule {}
