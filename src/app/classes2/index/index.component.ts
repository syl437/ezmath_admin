import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute} from "@angular/router";
import * as moment from 'moment';
import { MomentModule } from 'angular2-moment';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    public folderName:string = 'classes2';
    public addButton:string = 'הוסף שיעור'
    deleteModal: any;
    InfoModal:any;
    detailsModal: any;
    selectedItem: any;
    companyToDelete: any;
    UserContentInfo:any;
    SubCatId:any;
    PaymentModal:any;
    PaymentInfo:any;
    duplicateModal:any;
    DuplicateInfo:any;


    MissingModal:any;
    MissingContentInfo: any;

    returnModal: any;

    start = moment();
    end = moment();
    gap: string = 'days';
    sent: boolean = false;


    public senddetails:any = {
        'user_id' : '',
        'class_id' : '',
        'approve_status' : '',
        'payment_method' : ''
    }

    constructor(public MainService: MainService, settings: SettingsService , private modalService: NgbModal , private route: ActivatedRoute) {
        this.route.params.subscribe(params => {
            this.SubCatId = params['id'];
            if(!this.SubCatId)
                this.SubCatId = "-1";
            console.log("11 : " , this.SubCatId)
            this.host = settings.host;
            this.avatar = settings.avatar;
            //this.getItems();
            this.selectGap('days');
        });
    }

    ngOnInit() {
    }

    getItems()
    {
        this.MainService.GetClasses('GetClasses', this.SubCatId,this.start.format('YYYY-MM-DD'),this.end.format('YYYY-MM-DD') ).then((data: any) => {
            console.log("GetClasses : ", data)
            this.ItemsArray = data;
            this.ItemsArray1 = data;
        })
    }



    selectGap(gap: string){
        this.sent = false;
        this.gap = gap;
        switch (gap){
            case 'days':
                this.start = moment();
                this.end = moment();
                this.getItems();
                break;
            case 'weeks':
                this.start = moment().startOf('week');
                this.end = moment().endOf('week');
                this.getItems();
                break;
            case 'months':
                this.start = moment().startOf('month');
                this.end = moment().endOf('month');
                this.getItems();
                break;
            default:
                this.start = moment();
                this.end = moment();
                this.getItems();
                break;
        }

    }

    assignDates(settings: string){
        this.sent = false;
        if (settings === 'forward'){
            switch (this.gap){
                case 'days':
                    this.start = moment(Object.assign({}, this.start)).add(1, 'days');
                    this.end = moment(Object.assign({}, this.end)).add(1, 'days');
                    this.getItems();
                    break;
                case 'weeks':
                    this.start = moment(Object.assign({}, this.start)).add(1, 'weeks');
                    this.end = moment(Object.assign({}, this.end)).add(1, 'weeks');
                    this.getItems();
                    break;
                case 'months':
                    this.start = moment(Object.assign({}, this.start)).add(1, 'months');
                    this.end = moment(Object.assign({}, this.start)).endOf('month');
                    this.getItems();
                    break;
                default:
                    this.start = moment(Object.assign({}, this.start)).add(1, 'days');
                    this.end = moment(Object.assign({}, this.end)).add(1, 'days');
                    this.getItems();
                    break;
            }
        }
        if (settings === 'backward'){
            switch (this.gap){
                case 'days':
                    this.start = moment(Object.assign({}, this.start)).subtract(1, 'days');
                    this.end = moment(Object.assign({}, this.end)).subtract(1, 'days');
                    this.getItems();
                    break;
                case 'weeks':
                    this.start = moment(Object.assign({}, this.start)).subtract(1, 'weeks');
                    this.end = moment(Object.assign({}, this.end)).subtract(1, 'weeks');
                    this.getItems();
                    break;
                case 'months':
                    this.start = moment(Object.assign({}, this.start)).subtract(1, 'months');
                    this.end = moment(Object.assign({}, this.start)).endOf('month');
                    this.getItems();
                    break;
                default:
                    this.start = moment(Object.assign({}, this.start)).subtract(1, 'days');
                    this.end = moment(Object.assign({}, this.end)).subtract(1, 'days');
                    this.getItems();
                    break;
            }
        }
    }


    confirmClass(row,type)
    {
        this.senddetails.user_id = row.id;
        this.senddetails.class_id = this.UserContentInfo.id;
        this.senddetails.approve_status = type;


        this.MainService.confirmClass('ApproveClassesManager',this.senddetails,0).then((data: any) => {
            //console.log("ApproveClassesManager : " , data.json());
            row.confirmed_status = type;
        });
    }

    DeleteItem() {

        if (this.ItemsArray[this.companyToDelete].deleted == 0)
            this.ItemsArray[this.companyToDelete].deleted = 1;
        else {
            this.ItemsArray[this.companyToDelete].deleted = 0;
            //this.returnModal.close();
        }

        this.MainService.DeleteItem('DeleteClass', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
            //this.getItems();
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.title.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

    openDetailsModal(content, item){
        console.log("DM : " , content , item)
        this.detailsModal = this.modalService.open(content);
        this.selectedItem = item;
    }

    openInfoModal(content,row) {
        this.InfoModal = this.modalService.open(content);
        this.UserContentInfo = row;
        console.log("UserContentInfo",this.UserContentInfo)
    }

    openMissingModal(content,row)
    {
        this.MissingModal = this.modalService.open(content);
        this.MissingContentInfo = row;
        console.log("MissingContentInfo",this.MissingContentInfo)
    }

    openReturnModal(content, item) {
        this.returnModal = this.modalService.open(content);
        this.companyToDelete = item;
    }


    openPaymentModal(content,row) {
        this.PaymentModal = this.modalService.open(content);
        this.PaymentInfo = row;
        console.log("UserContentInfo",this.PaymentInfo)
    }

    OpenDuplicateClassModal(content,row) {
        this.duplicateModal = this.modalService.open(content);
        this.DuplicateInfo = row;
        console.log("DuplicateInfo",this.DuplicateInfo)
    }

    duplicateClassYear() {

        let duplicateId = '';
        if (this.DuplicateInfo.duplicate_id > 0)
            duplicateId = this.DuplicateInfo.duplicate_id;
        else
            duplicateId = this.DuplicateInfo.id;



        this.MainService.duplicateClass('duplicateClassYear', duplicateId).then((data: any) => {
            this.duplicateModal.close();
            if (this.DuplicateInfo.class_duplicated == 0)
                this.DuplicateInfo.class_duplicated = 1;
            else
                this.DuplicateInfo.class_duplicated = 0;

            this.getItems();
        })
    }


    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }

}
