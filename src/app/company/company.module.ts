import {NgModule, Pipe} from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DragulaModule } from 'ng2-dragula/ng2-dragula';


import { CompanydRoutes } from './company.routing';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';



import {CompanyService} from "./company.service";
import {HttpModule} from "@angular/http";
import {LineaComponent} from "../icons/linea/linea.component";
import {SliComponent} from "../icons/sli/sli.component";
import { EditComponent } from './edit/edit.component';
import { IndexComponent } from './index/index.component';
import { AddComponent } from './add/add.component';
import {ButtonIconsComponent} from "../components/button-icons/button-icons.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FileUploadModule} from "ng2-file-upload";
import {FormsModule, NgControl} from "@angular/forms";
import {SearchFilterPipe} from "./pipe.service";



@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(CompanydRoutes),
        NgxDatatableModule,
        HttpModule,
        NgbModule,
        FileUploadModule,
        FormsModule
    ],
    declarations: [
        EditComponent,
        IndexComponent,
        AddComponent,
        SearchFilterPipe
    ],
    providers: [CompanyService]
})


export class CompanyModule {}
