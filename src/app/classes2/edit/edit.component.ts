import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder,FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {Ng2UploaderModule} from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbDatepicker, NgbDateStruct, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';



@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent implements OnInit {
    registerForm: FormGroup;
    public navigateTo:string = '/classes2/index';
    public paramsSub;
    public id: number;
    public SubCategories;
    public isReady;
    public imageSrc: string = '';
    public folderName:string = 'classes2';
    //public rowsNames: any[] = ['כותרת','כמות'];
    //public rows: any[] = ['title','quan'];
    now = new Date();
    time: NgbTimeStruct = {hour: 13, minute: 30, second: 30};

    public fields:any = {
        "id" : "",
        "title" : "",
        "max_students" : "1",
        "class_date" : "",
        "teacher_id" : "0",
        "branchselect" : "",
        "professionselect" : "",
        "class_time" : this.time,
        "class_total_hours" : "1"
    }

    public Id;
    public Item;
    public host;

    public sub;
    public BranchesItems: any[];
    public ProfessionsItems: any[];
    public teachersArray: any = [];
    public rowIndex:any = '';


    @ViewChild("fileInput") fileInput;
    @ViewChild('dp') dp: any;

    constructor(private route: ActivatedRoute, private http: Http, public service: MainService, public router: Router, public settings: SettingsService) {

        this.route.params.subscribe(params => {
            this.Id = params['id'];

            for (let i = 0; i < this.service.Items.length; i++) {
                if (this.service.Items[i].id == this.Id) {
                    this.rowIndex = i;
                }
            }

            this.Item = this.service.Items[this.rowIndex];

            //this.Item = this.service.Items[this.Id];
            this.host = settings.host;
            //this.Item.Change  = false;
            //this.isReady = true;
            console.log("Product", this.Item)
            console.log("Item : " , this.Item)
        });
    }

    onSubmit(form: NgForm) {


        //let fi = this.fileInput.nativeElement;
        let fileToUpload;
        /*
        if (fi.files && fi.files[0]) {fileToUpload = fi.files[0]; console.log("fff : ",fileToUpload);}
        this.Item.Change = this.Change;
        */
        console.log("EditClass",form.value);
        this.service.EditItem('EditClass',this.fields,fileToUpload).then((data: any) => {
            console.log("EditClass : " , data);
            this.router.navigate([this.navigateTo]);
        });

    }




    getBranches()
    {
        this.service.GetBranches('GetBranches').then((data: any) => {
            console.log("GetBranches : " , data);
            this.BranchesItems = data;
        });
    }

    GetProfessions()
    {
        this.service.GetProfessions('GetProfessions').then((data: any) => {
            console.log("GetProfessions : " , data);
            this.ProfessionsItems = data;
        });
    }

    async GetTeachers()
    {
        await this.service.GetTeachers('webGetTeachers').then((data: any) => {
            console.log("webGetTeachers : " , data);
            this.teachersArray = data;
        });
    }

    ngOnInit() {

        let splitdate = this.Item.class_date.split("-");
        let splithour = this.Item.class_hour.split(":");

        this.fields.id = this.Item.id;
        this.fields.title = this.Item.title;
        this.fields.max_students = this.Item.quan;
        this.fields.branchselect = this.Item.branch_id;
        this.fields.teacher_id = this.Item.teacher_id;
        this.fields.professionselect = this.Item.profession_id;
        this.fields.class_total_hours = this.Item.class_total_hours;
        this.fields.class_time = {hour: parseInt(splithour[0]), minute: parseInt(splithour[1]), second: '00'};
        this.fields.class_date = {year: parseInt(splitdate[0]), month: parseInt(splitdate[1]), day: parseInt(splitdate[2])};
        //this.dp.navigateTo()


        this.getBranches();
        this.GetProfessions();
        this.GetTeachers();


    }



}
