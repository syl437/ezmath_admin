import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute} from "@angular/router";
import {FormBuilder, FormControl , FormGroup, NgForm, Validators} from "@angular/forms";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {
    //registerForm: FormGroup;
    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    public folderName:string = 'push';
    public addButton:string = ''
    deleteModal: any;
    detailsModal: any;
    selectedItem: any;
    companyToDelete: any;
    SubCatId:any;
    noResults : boolean = false;
    resultsFound : boolean = false;
    SendButtonActive : boolean = false;


    public fields : any = {
        "school_name" : "",
        "userType" : "0",
        "branchselect" : "0",
        "schoolgrade" : "0",
        "city" : "",
        "teachinglevel" : "0",
    }

    public schoolgradeArray : any = [];
    public teachinglevelArray : any = [];
    public branchesArray : any = [];
    public searchResultsArray : any = [];

    pushModal: any;
    companyToPush: any;
    pushText: string = '';
    sent: boolean = false;
    inProcess: boolean = false;
    UserType :any = ["תלמיד","מורה",'מנהל'];


    constructor(public service:MainService, settings: SettingsService , private modalService: NgbModal , private route: ActivatedRoute) {
        this.route.params.subscribe(params => {
            this.SubCatId = params['id'];
            if(!this.SubCatId)
                this.SubCatId = "-1";
            console.log("11 : " , this.SubCatId)
            this.host = settings.host;
            this.avatar = settings.avatar;

            this.getSchoolGrade();
            this.getTeachingLevel();
            this.getBranches();
        });
    }

    async getSchoolGrade() {
        await this.service.GetItems('webGetSchoolGrade',-1).then((data: any) => {
            this.schoolgradeArray = data;
            console.log("webGetSchoolGrade : " , data);
        });
    }

    async getTeachingLevel() {
        await this.service.GetItems('webGetTeachingLevel',-1).then((data: any) => {
            console.log("webGetTeachingLevel : " , data);
            this.teachinglevelArray = data;
        });
    }

    async getBranches() {
        await this.service.GetItems('GetBranches',-1).then((data: any) => {
            console.log("GetBranches : " , data);
            this.branchesArray = data;
        });
    }

    searchForm()
    {

        this.noResults = false;
        this.resultsFound = false;

        this.service.SearchUsersPush('WebSearchUsersPush',this.fields).then((data: any) => {
            console.log("WebSearchUsersPush:",data);
            this.searchResultsArray = data;
            if (this.searchResultsArray.length == 0){
                this.noResults = true;
                //alert ("לא נמצאו תוצאות חיפוש");
            }
            else {
                this.resultsFound = true;
                this.SendButtonActive = true;
            }
        });

    }

    changeSelection(row) {
       if (row.choosen == 0)
           row.choosen = 1;
       else
           row.choosen = 0;
    }



    openPushModal (content, company) {

        let selectedCount = 0;

        for (let i = 0; i < this.searchResultsArray.length; i++) {
            if (this.searchResultsArray[i].choosen == 1) {
                selectedCount++;
            }
        }

        if (selectedCount == 0) {
            alert ("יש לסמן משתמש אחד לפחות לשליחת הודעה")
        }
        else {
            this.pushModal = this.modalService.open(content);
        }
    }

    closePushModal () {
        this.pushModal.close();
        this.sent = true;
    }

    sendPush() {

        this.inProcess = true;
        this.sent = false;
        this.service.SendUsersPush('SendUsersPush',this.searchResultsArray,this.pushText).then((data: any) => {

            this.pushText = '';
            this.sent = true;
            this.inProcess = false;
            //this.closePushModal();

            //this.searchResultsArray = [];
            //this.noResults = false;
            ///this.resultsFound = false;
            //this.SendButtonActive  = false;
            console.log("SendUsersPush:",data);
        });
    }

    ngOnInit() {

    }





}
