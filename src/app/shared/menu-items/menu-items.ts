import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: '/',
    name: 'ראשי',
    type: 'link',
    icon: 'basic-accelerator'
  },{
        state: 'schoolgrade',
        child: 'index',
        name: 'כיתות',
        type: 'new',
        icon: 'basic-message-txt'
    },{
        state: 'teachinglevel',
        child: 'index',
        name: 'רמת לימוד',
        type: 'new',
        icon: 'basic-message-txt'
    },{
        state: 'users',
        child: 'index',
        name: 'משתמשים',
        type: 'new',
        icon: 'basic-message-txt'
    },{
        state: 'users_debt',
        child: 'index',
        name: 'תלמידים בחוב',
        type: 'new',
        icon: 'basic-message-txt'
    },{
        state: 'tickets',
        child: 'index',
        name: 'כרטיסיות',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'professions',
        child: 'index',
        name: 'מקצועות',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'branches',
        child: 'index',
        name: 'סניפים',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'classes2',
        child: 'index',
        name: 'שיעורים',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'user_classes',
        child: 'index',
        name: 'הרשמות לשיעורים',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'ticket_groups',
        child: 'index',
        name: 'קבוצת כרטיסיות',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'class_contact_leads',
        child: 'index',
        name: 'צור קשר שיעור',
        type: 'new',
        icon: 'basic-message-txt'
    },
/*
    {
        state: 'classes',
        child: 'index',
        name: 'שיעורים',
        type: 'new',
        icon: 'basic-message-txt'
    },
 */
    {
        state: 'push',
        child: 'index',
        name: 'הודעות פוש',
        type: 'new',
        icon: 'basic-message-txt'
    }
];

 @Injectable()
export class MenuItems {
  getAll(): Menu[] {
       return MENUITEMS;
     }

  add(menu: Menu) {
    MENUITEMS.push(menu);
  }
}
